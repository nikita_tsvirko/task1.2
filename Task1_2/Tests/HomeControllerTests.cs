﻿using Services;
using Repository;
using App.Controllers;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using AutoMapper;
using Xunit;
using Moq;

namespace Tests
{
    public class HomeControllerTests
    {
        [Fact]
        public void GetIndexReturn()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.Index();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void PostIndexInvalidProduct()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);
            var newProduct = new ProductViewModel();
            controller.ModelState.AddModelError("Name", "StringLength");

            // Act
            var result = controller.Index(newProduct);

            // Assert
            var contentResult = Assert.IsType<ContentResult>(result);
            Assert.Equal("Данные были введены некорректно", contentResult.Content);
        }

        [Fact]
        public void PostIndexNullProduct()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.Index(null);

            // Assert
            var contentResult = Assert.IsType<ContentResult>(result);
            Assert.Equal("Данные были введены некорректно", contentResult.Content);
        }

        [Fact]
        public void PostIndexValidProduct()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);
            var newProduct = new ProductViewModel()
            {
                Name = "TestName",
                Info = "TestInfo",
                Date = DateTime.Now,
                Country = "Belarus",
                Price = 2443
            };

            // Act
            var result = controller.Index(newProduct);

            // Assert
            var contentResult = Assert.IsType<ContentResult>(result);
            Assert.Equal("Товар успешно создан", contentResult.Content);
        }

        [Fact]
        public void GetList()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            service.Setup(opt => opt.TakeFromStorage(It.IsAny<int>())).Returns(TestProducts());
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.List();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IService>(viewResult.Model);
            Assert.Equal(model.TakeFromStorage().Count, TestProducts().Count);
        }

        [Fact]
        public void GetListProcessing()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            service.Setup(opt => opt.TakeFromStorage(It.IsAny<int>())).Returns(TestProducts());
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.ListProcessing();

            // Assert
            var viewResult = Assert.IsType<PartialViewResult>(result);
            var model = Assert.IsAssignableFrom<IService>(viewResult.Model);
            Assert.Equal(TestProducts().Count, model.TakeFromStorage((int)viewResult.ViewData["page"]).Count);
        }

        [Fact]
        public void GetListProcessingFailCountOnSecondPage()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            service.Setup(opt => opt.TakeFromStorage(It.Is<int>(x => x == 1))).Returns(TestProducts());
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.ListProcessing(page: 2);

            // Assert
            var viewResult = Assert.IsType<PartialViewResult>(result);
            var model = Assert.IsAssignableFrom<IService>(viewResult.Model);
            Assert.Equal(TestProducts().Count, model.TakeFromStorage((int)viewResult.ViewData["page"]).Count);
        }

        [Fact]
        public void GetSort()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.Sort();

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("ListProcessing", viewResult.ActionName);
            service.Verify(sort => sort.SortStorageByNameAsc());
        }

        [Fact]
        public void GetSortFail()
        {
            // Arrange
            var mapper = new Mock<IMapper>();
            var service = new Mock<IService>();
            var controller = new HomeController(mapper.Object, service.Object);

            // Act
            var result = controller.Sort(SortState.DateAsc);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("ListProcessing", viewResult.ActionName);
            service.Verify(sort => sort.SortStorageByNameAsc());
        }

        private List<Product> TestProducts()
        {
            var products = new List<Product>
            {
                new Product { Id=1, Name="Product1", Info = "Info1", Date = DateTime.Now, Country= "BLR", Price=700},
                new Product { Id=2, Name="Product2", Info = "Info2", Date = DateTime.Now, Country= "RUS", Price=200},
                new Product { Id=3, Name="Product3", Info = "Info3", Date = DateTime.Now, Country= "UKR", Price=500}
            };
            return products;
        }
    }
}
