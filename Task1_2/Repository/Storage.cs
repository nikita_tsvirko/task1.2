﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class Storage : IStorage
    {
        private static SortType sortState = SortType.NameAsc;
        private static ProductsContext dbProducts;

        public Storage(ProductsContext _dbProducts)
        {
            dbProducts = _dbProducts;
        }

        public void SortByNameAsc() => sortState = SortType.NameAsc;
        public void SortByNameDesc() => sortState = SortType.NameDesc;
        public void SortByDateAsc() => sortState = SortType.DateAsc;
        public void SortByDateDesc() => sortState = SortType.DateDesc;

        public void Add(Product product)
        {
            using (dbProducts)
            {
                dbProducts.Add(product);
                dbProducts.SaveChanges();
            }
        }

        public List<Product> Take(int numOfSkip, int numOfTake)
        {
            var result = dbProducts.Products.ToList();
            switch (sortState)
            {
                case SortType.NameDesc:
                    result = result.OrderByDescending(x => x.Name).ToList();
                    break;
                case SortType.DateAsc:
                    result = result.OrderBy(x => x.Date).ToList();
                    break;
                case SortType.DateDesc:
                    result = result.OrderByDescending(x => x.Date).ToList();
                    break;
                default:
                    result = result.OrderBy(x => x.Name).ToList();
                    break;
            }
            return result.Skip(numOfSkip).Take(numOfTake).ToList();
        }

        public List<Product> GetForDeleteByDate(DateTime deleteBoundary)
        {
            var result = dbProducts.Products.Where(p => p.Date < deleteBoundary).ToList();
            return result;  
        }

        public async Task DeleteProduct(int id)
        {
            Product product = dbProducts.Products.FirstOrDefault(p => p.Id == id);
            if (product != null) dbProducts.Entry((object) product).State = EntityState.Deleted;
            await dbProducts.SaveChangesAsync();
        }

        public int CountItems()
        {
            return dbProducts.Products.Count();
        }
    }
}