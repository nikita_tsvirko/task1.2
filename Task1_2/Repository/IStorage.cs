﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IStorage
    {
        void SortByNameAsc();
        void SortByNameDesc();
        void SortByDateAsc();
        void SortByDateDesc();

        void Add(Product product);
        List<Product> Take(int numOfSkip, int numOfTake);
        List<Product> GetForDeleteByDate(DateTime deleteBoundary);
        Task DeleteProduct(int id);
        int CountItems();
    }
}
