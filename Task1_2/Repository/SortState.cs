﻿namespace Repository
{
    public enum SortType
    {
        NameAsc,
        NameDesc,
        DateAsc,
        DateDesc
    }
}