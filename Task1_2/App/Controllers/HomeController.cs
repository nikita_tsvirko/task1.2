﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;
using Services;
using App.Models;
using AutoMapper;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace App.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IService _service;

        public HomeController(IMapper mapper, IService service)
        {
            _service = service;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(ProductViewModel product)
        {
            if (product != null && ModelState.IsValid)
            {
                _service.AddToStorage(_mapper.Map<Product>(product));
                return Content("Товар успешно создан");
            }
            return Content("Данные были введены некорректно");
        }

        public async Task<IActionResult> List(int page = 1)
        {
            ViewData["Page"] = page;
            return View(_service);
        }

        public IActionResult ListProcessing(int page = 1, SortState sortState = SortState.NameAsc)
        {
            ViewData["Page"] = page;
            ViewBag.Name = sortState == SortState.NameDesc ? SortState.NameDesc : SortState.NameAsc;
            ViewBag.Date = sortState == SortState.DateDesc ? SortState.DateDesc : SortState.DateAsc;
            return PartialView(_service);
        }

        public IActionResult Sort(SortState sortState = SortState.NameAsc)
        {
            SortState newSortState;

            if (sortState == SortState.NameAsc || sortState == SortState.NameDesc)
            {
                newSortState = sortState == SortState.NameAsc ? SortState.NameDesc : SortState.NameAsc;
            }
            else
            {
                newSortState = sortState == SortState.DateAsc ? SortState.DateDesc : SortState.DateAsc;
            }

            switch (sortState)
            {
                case SortState.NameDesc:
                    _service.SortStorageByNameDesc();
                    break;
                case SortState.DateAsc:
                    _service.SortStorageByDateAsc();
                    break;
                case SortState.DateDesc:
                    _service.SortStorageByDateDesc();
                    break;
                default:
                    _service.SortStorageByNameAsc();
                    break;
            }

            return RedirectToAction("ListProcessing", new {sortState = newSortState});
        }
    }
}