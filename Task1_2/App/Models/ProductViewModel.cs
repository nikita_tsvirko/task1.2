﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public class ProductViewModel
    {
        [Required(ErrorMessage = "Укажите имя товара")]
        [StringLength(50, ErrorMessage = "Имя товара превышает максимальный размер")]
        [RegularExpression(@"[A-Za-z0-9А-Яа-я]*", ErrorMessage = "В имени допустимо использование букв и цифр")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите описание товара")]
        [StringLength(256, ErrorMessage = "Описание товара превышает максимальный размер")]
        [DataType(DataType.MultilineText)]
        public string Info { get; set; }

        [Required(ErrorMessage = "Укажите дату получения товара")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Укажите страну изготовления товара")]
        [StringLength(30, ErrorMessage = "Страна изготовления товара превышает максимальный размер")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Укажите цену товара")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Можно использовать только цифры")]
        public int Price { get; set; }
    }
}
