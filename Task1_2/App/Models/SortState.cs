﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Models
{
    public enum SortState
    {
        NameAsc,
        NameDesc,
        DateAsc,
        DateDesc
    }
}