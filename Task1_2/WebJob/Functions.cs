﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;
using Services;

namespace WebJob
{
    public class Functions
    {
        private readonly IService _service;

        public Functions(IService service)
        {
            _service = service;
        }

        public async Task FunctionOnATimer([TimerTrigger("20 * * * * *", RunOnStartup = true)]
            TimerInfo timerInfo, TextWriter log)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            var deleted = _service.TakeOldFromStorage();
            if (deleted != null)
            {
                await _service.DeleteOldInStorage();
                string body = "<table>";
                foreach (var product in deleted)
                {
                    body += $"<tr> <td>{product.Name}</td> <td>{product.Info}</td> " +
                            $"<td>{product.Date}</td> <td>{product.Price}</td> <td>{product.Country}</td></tr>";
                }

                body += "</table>";
                MailMessage msg = new MailMessage();
                msg.To.Add(new MailAddress(configuration.GetConnectionString("EmailTo")));
                msg.From = new MailAddress(configuration.GetConnectionString("EmailFrom"));
                msg.Subject = "Delete in database";
                msg.Body = body;
                msg.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(configuration.GetConnectionString("EmailAccount"),
                        configuration.GetConnectionString("Password"))
                };
                try
                {
                    client.Send(msg);
                }
                catch (Exception e)
                {
                    log.WriteLine(e);
                }
            }

            log.WriteLine($"Deleted in database: {deleted.Count}");
        }
    }
}