﻿using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IService
    {
        void SortStorageByNameAsc();
        void SortStorageByNameDesc();
        void SortStorageByDateAsc();
        void SortStorageByDateDesc();

        void SetPageSize(int newPageSize);
        void AddToStorage(Product product);
        void CheckPreviosAndNextPage(int page);
        List<Product> TakeFromStorage(int page = 1);
        List<Product> TakeOldFromStorage();
        Task DeleteOldInStorage();
    }
}
