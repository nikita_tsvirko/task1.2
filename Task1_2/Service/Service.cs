﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository;

namespace Services
{
    public class Service : IService
    {
        public IStorage storage;
        private int pageSize = 3;
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }

        public void SortStorageByNameAsc() => storage.SortByNameAsc();
        public void SortStorageByNameDesc() => storage.SortByNameDesc();
        public void SortStorageByDateAsc() => storage.SortByDateAsc();
        public void SortStorageByDateDesc() => storage.SortByDateDesc();

        public Service(IStorage storage)
        {
            this.storage = storage;
        }

        public void SetPageSize(int newPageSize) => pageSize = newPageSize;

        public void AddToStorage(Product product)
        {
            storage.Add(product);
        }

        public void CheckPreviosAndNextPage(int page)
        {
            HasPreviousPage = page > 1;
            HasNextPage = page < TotalPages;
        }
        
        public List<Product> TakeFromStorage(int page = 1)
        {
            TotalPages = (int)Math.Ceiling(storage.CountItems() / (double)pageSize);
            CheckPreviosAndNextPage(page);
            var result = storage.Take((page - 1) * pageSize, pageSize);
            return result;
        }

        public List<Product> TakeOldFromStorage()
        {
            DateTime mondayBeforeLastMonday = DateTime.Today;
            mondayBeforeLastMonday = mondayBeforeLastMonday.AddDays((double)(0 - (mondayBeforeLastMonday.DayOfWeek) - 6));
            var result = storage.GetForDeleteByDate(mondayBeforeLastMonday);
            return result;
        }

        public async Task DeleteOldInStorage()
        {
            var deleteProducts = TakeOldFromStorage();
            foreach (var product in deleteProducts)
            {
                await storage.DeleteProduct(product.Id);
            }
        }
    }
}
